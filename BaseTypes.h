//
//  BaseTypes.h
//  cpp11-experiments
//
//  Created by zen on 18/8/14.
//  Copyright (c) 2014 Gutsy Games LLP. All rights reserved.
//

#ifndef cpp11_experiments_BaseTypes_h
#define cpp11_experiments_BaseTypes_h

#include <cstdint>

using BOOL = bool;
using BYTE = unsigned char;

using INT32 = int32_t;
using UINT32 = uint32_t;
using INT64 = int64_t;
using UINT64 = uint64_t;

using SHORT = INT32;
using USHORT = UINT32;
using INT = INT64;
using UINT = UINT64;

// an explanation here: http://www.viva64.com/en/a/0050/
using INT_PTR = intptr_t;
using UINT_PTR = uintptr_t;

using FLOAT = float;
using DOUBLE = double;

#endif
