//
//  Main.cpp
//  cpp11-experiments
//
//  Created by zen on 17/8/14.
//  Copyright (c) 2014 Gutsy Games LLP. All rights reserved.
//
#include <iostream>
#include "BaseIncludes.h"
#include "Delegate.h"
#include "SomeClass.h"

namespace
{
    int FreeFunc(void* pPtr, int val)
    {
        std::cout << "called free anonymous-namespaced empty func " << __PRETTY_FUNCTION__ << "with val:" << val << std::endl;
        
        return val;
    }
    
    void Func()
    {
        std::cout << "called free anonymous-namespaced empty func" << std::endl;
    }
}

static void StaticEmptyFunc()
{
    std::cout << "called static empty func" << std::endl;
}

int main()
{
    SomeClass someObj;
    
    using ParseDelegate = Delegate<int, int>;
    
    auto p = ParseDelegate::Create(FreeFunc);
    
    auto d = Delegate<int, int>::Create<SomeClass, &SomeClass::Foo>(&someObj);
    auto d2 = Delegate<void, float, void*>::Create<SomeClass, &SomeClass::Bar>(&someObj);
    
    p(116);
    d(42);
    d2(69, nullptr);

    
    return 0;
}
