//
//  SomeClass.h
//  cpp11-experiments
//
//  Created by zen on 6/10/14.
//  Copyright (c) 2014 Gutsy Games LLP. All rights reserved.
//

#ifndef __cpp11_experiments__SomeClass__
#define __cpp11_experiments__SomeClass__

#include <iostream>

class SomeClass
{
public:
    int Foo(int x)
    {
        std::cout << "foo called with " << x << std::endl;
        return x;
    }
    
    void Bar(float f, void* p)
    {
        std::cout << "foo called with " << f << std::endl;
        if (p) {
            std::cout << "p is not null" << p << std::endl;
        } else {
            std::cout << "p is null" << p << std::endl;
        }
    }
};

#endif /* defined(__cpp11_experiments__SomeClass__) */
