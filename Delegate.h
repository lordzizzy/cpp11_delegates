//
//  Delegate.h
//  cpp11-experiments
//
//  Created by zen on 17/8/14.
//  Copyright (c) 2014 Gutsy Games LLP. All rights reserved.
//

//NOTE: Previous implementation is taken from molecular musings blog http://molecularmusings.wordpress.com (check git :))
// Latest implementation derived from http://blog.coldflake.com/posts/2014-01-12-C++-delegates-on-steroids.html

#ifndef cpp11_experiments_Delegate_h
#define cpp11_experiments_Delegate_h

#include <utility>
#include <cassert>

//referencing techniques used in this article: http://www.codeproject.com/Articles/11015/The-Impossibly-Fast-C-Delegates

template <class TReturnType, class... TParams>
class Delegate
{
    using Callback = TReturnType (*)(void* callee, TParams...);
    
public:
    // constructor(s)
    Delegate(void* callee, Callback callback) :
        m_callee(callee),
        m_callback(callback)
    {}
    
    // factory function(s)
    template <class T, TReturnType (T::*Method)(TParams...)>
    static Delegate Create(T* callee)
    {
        return Delegate(callee, &Invoke<T, Method>);
    }
    
    static Delegate Create(Callback callback)
    {
        return Delegate(nullptr, callback);
    }
    
    // function call operator
    TReturnType operator()(TParams... params) const
    {
        return m_callback(m_callee, params...);
    }
    
private:
    // internal invoke function
    template <class T, TReturnType (T::*Method)(TParams...)>
    static TReturnType Invoke(void* callee, TParams... params)
    {
        T* p = static_cast<T*>(callee);
        return (p->*Method)(params...);
    }
    
private:
    void*       m_callee;
    Callback    m_callback;
};

#endif
