//
//  BaseIncludes.h
//  cpp11-experiments
//
//  Created by zen on 17/8/14.
//  Copyright (c) 2014 Gutsy Games LLP. All rights reserved.
//

#ifndef cpp11_experiments_BaseIncludes_h
#define cpp11_experiments_BaseIncludes_h

#include "BaseTypes.h"

#define ALWAYS_UNUSED(x) do { (void)(x); } while (0);

#endif
